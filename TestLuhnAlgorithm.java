import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TestLuhnAlgorithm {
	private static Scanner scanner = new Scanner(System.in);
	private static int numAll = 0;

	public static void main(String[] args){
	    System.out.println("<< Luhn Algorithm >>");
        System.out.print("Jumlah Kartu :" );
        String inputJumlahKartu = scanner.nextLine();
        int inputJumlahKartuNew = Integer.valueOf(inputJumlahKartu);
        List<String> listOfCreditCard = new ArrayList<String>();
        
        if (inputJumlahKartuNew <= 10) {
	        for (int z=0; z<inputJumlahKartuNew; z++){
	        	System.out.print("\n");
	        	System.out.print("Kartu "+(z+1)+">");
	        	String newCard = scanner.nextLine();
	        	listOfCreditCard.add(newCard);
	        	
	        	if (z+1==inputJumlahKartuNew){
	        		for (int y=0; y<listOfCreditCard.size(); y++){
	        			luhnProcess(listOfCreditCard.get(y), y);
	        		}
	        	}
	        }	        
        } else {
        	System.out.print("Maaf,tidak bisa di proses, maksimum 10 nomor kartu" );
        }
	}
	
	private static void luhnProcess(String cc, int iterate){
		String ccReplaceAll = cc.replaceAll("\\-", "");
		System.out.println("===========================================");
		
		if (!ccReplaceAll.contains("?")){
			luhnEngine(cc, null);
		} else {
			for(int z=0; z<=9 ; z++){
				luhnEngine(cc, String.valueOf(z));							
			}
		}
		System.out.println("Output Kartu "+(iterate+1)+"> "+numAll);
		numAll = 0;
	}
	
	public static void luhnEngine(String cc, String index){
		String ccReplaceAll = cc.replaceAll("\\-", "");
		String ccReplaceAllNew = "";
		int iter = 0;
		int ccIntTot = 0;
		int ccIntTotAll = 0;
		int ccIntNonModTot = 0;
		int resNumTot = 0;
		
		for (int curs=ccReplaceAll.length()-2; curs>=0; curs--){
			if (index == null)
				ccReplaceAllNew = ccReplaceAll;
			else
				ccReplaceAllNew = ccReplaceAll.replaceAll("\\?", index);
			
			if (iter%2 == 0) {
				String ccString = String.valueOf(ccReplaceAllNew.charAt(curs));
				int ccInt = Integer.parseInt(ccString) * 2;
				if ((Integer.parseInt(ccString) * 2) > 9) {
					String stringNumFirst = String.valueOf(String.valueOf(ccInt).charAt(0));
					String stringNumSec = String.valueOf(String.valueOf(ccInt).charAt(1));
					int intNumFirst = Integer.parseInt(stringNumFirst);
					int intNumSec = Integer.parseInt(stringNumSec);
					int resNum = intNumFirst + intNumSec;					
					resNumTot = resNum;
					ccIntTotAll = ccIntTotAll + resNumTot;
				} else {
					ccIntTot = ccInt;
					ccIntTotAll = ccIntTotAll + ccIntTot;
				}
				
			} else {
				String ccStringNonMod = String.valueOf(ccReplaceAllNew.charAt(curs));
				int ccIntNonMod = Integer.parseInt(ccStringNonMod);
				ccIntNonModTot = ccIntNonModTot + ccIntNonMod;
			}
			iter++;
		}
		int res = ccIntTotAll + ccIntNonModTot;
		String stringResSec = String.valueOf(String.valueOf(res).charAt(1));
		int intResSec = Integer.parseInt(stringResSec);
		int minus = 0;
		if (!stringResSec.equals("0"))
			minus = 10 - intResSec;
		if (((minus + res) % 10) == 0 ) {
			if (String.valueOf(ccReplaceAll.charAt(ccReplaceAll.length()-1)).equals(String.valueOf(minus))){
				System.out.println(ccReplaceAllNew +" (Valid)");
				numAll++;
			}
			else {
				System.out.println(ccReplaceAllNew +" (Not Valid)");
			}
		} else {
			System.out.println(ccReplaceAllNew +" (Not Valid)");
		}
		ccIntTotAll = 0;
		ccIntNonModTot = 0;
		iter = 0;		
	}
}
